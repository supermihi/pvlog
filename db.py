from datetime import date, timedelta
from typing import List, Dict

from client import PvLogClient, PvLogError, DayYield
from plant import Plant


class PlantDatabase:
    def __init__(self, plant: Plant, yields: List[DayYield] = None):
        self.yields = yields or []
        self.plant = plant

    @staticmethod
    def from_json(data: dict, plants: Dict[str, Plant]):
        entries = [DayYield.from_json(e) for e in data['yields']]
        plant = plants[data['plant']]
        return PlantDatabase(plant, entries)

    def to_json(self):
        return dict(plant=self.plant.name,
                    yields=[y.to_json() for y in self.yields])

    def missing_days(self, target=None):
        one_day = timedelta(days=1)
        target = target or (date.today() - one_day)
        day = (self.yields[-1].date + one_day) if len(self.yields) > 0 else self.plant.first_entry
        while day <= target:
            yield day
            day = day + one_day

    def fetch_missing(self, client: PvLogClient):
        missing_days = list(self.missing_days())
        if len(missing_days) == 0:
            print(f'plant database for {self.plant} is up to date')
            return
        missing_months = set((d.year, d.month) for d in missing_days)
        try:
            for year, month in sorted(missing_months):
                result = client.show_month(self.plant, year, month)
                for day_yield in sorted(result):
                    if day_yield.date in missing_days:
                        self.yields.append(day_yield)
        except PvLogError as e:
            print(str(e))
            return

