import json
from collections import OrderedDict
from pathlib import Path
from typing import Dict

from client import PvLogClient
from db import PlantDatabase
from excel import PvExcelDatabase
from settings import Settings


def run():
    settings_path = Path('.') / 'settings.json'
    settings = Settings.from_json(json.loads(settings_path.read_text()))
    client = PvLogClient(settings.api_key)

    plant_dbs: Dict[str, PlantDatabase] = OrderedDict([( name, PlantDatabase(plant)) for name, plant in settings.plants.items()])
    db_path = Path('.') / 'db.json'
    dbs_json = json.loads(db_path.read_text()) if db_path.exists() else []
    for db_json in dbs_json:
        db = PlantDatabase.from_json(db_json, settings.plants)
        plant_dbs[db.plant.name] = db

    for db in plant_dbs.values():
        db.fetch_missing(client)

    print('storing modified json database ...')
    db_path.write_text(json.dumps([db.to_json() for db in plant_dbs.values()], indent=2))
    print('done.')

    excel_path = Path('.') / 'lauterstrom.xlsx'
    excel_db = PvExcelDatabase(excel_path)
    print('storing yields to excel file ...')
    excel_db.store_yields(list(plant_dbs.values()))
    excel_db.save()
    print('done.')




    #pprint.pprint(client.plant_info(spicherer))
    #pprint.pprint(client.show_day(spicherer, date(2017, 12, 30)))
    #pprint.pprint(client.show_months(spicherer))
    #for plant in plants:
    #    yields = client.get_years(plant, [2017, 2018])
    #   to_csv(yields)


if __name__ == '__main__':
    run()