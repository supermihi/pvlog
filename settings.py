from typing import List, Dict

from plant import Plant


class Settings:

    api_key: str
    plants: Dict[str, Plant]

    def __init__(self, api_key: str, plants: Dict[str, Plant]):
        self.api_key = api_key
        self.plants = plants

    @staticmethod
    def from_json(data: dict):
        api_key = data['api_key']
        plants = [Plant.from_json(j) for j in data['plants']]
        plants_dict = {plant.name: plant for plant in plants}
        return Settings(api_key, plants_dict)
