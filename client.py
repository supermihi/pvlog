from datetime import date
from functools import total_ordering
from numbers import Integral, Number
from typing import List

import requests
from dateutil.parser import parse

from plant import Plant

BASE_URI = 'https://api.pv-log.com/api/v1'


class PvLogError(Exception):
    pass


@total_ordering
class DayYield:
    def __init__(self, date: date, watthours: Number):
        self.date = date
        self.watthours = watthours

    @staticmethod
    def from_json(data: dict):
        return DayYield(parse(data['date']).date(), data['yield'])

    def to_json(self):
        return {
            'date': self.date.strftime('%Y-%m-%d'),
            'yield': self.watthours
        }

    def __lt__(self, other):
        return self.date <=  other.date

    def __eq__(self, other):
        return self.date == other.date


class PvLogClient:

    def __init__(self, api_key: str):
        self.api_key = api_key
        self.base_uri = BASE_URI

    @property
    def root(self):
        return f'{self.base_uri}/{self.api_key}'

    def make_url(self, suffix: str):
        return f'{self.root}/{suffix}'

    def help(self):
        return requests.get(self.make_url('help')).json()

    def plant_info(self, plant: Plant):
        uri = self.make_url(f'plant/show/{plant.key}')
        return requests.get(uri).json()['result']

    def get_result(self, ans: dict):
        message = ans['messages'][0]
        if message['typestatus'] != 'success':
            raise PvLogError(f'API error: {message["title"]}')
        return ans['result']

    def show_day(self, plant: Plant, date: date):
        uri = self.make_url(f'plant/yield/show/days/{date.year}/{date.month}/{date.day}/{plant.key}')
        print(f'fetching yield on {date} for {plant}')
        ans = self.get_result(requests.get(uri).json())

        watthours = ans['yields']['watthours']
        if watthours is None:
            raise RuntimeError(f'No value for {plant} on {date}')
        return DayYield(date, watthours)

    def show_month(self, plant: Plant, year: int, month: int) -> List[DayYield]:
        uri =self.make_url(f'plant/yield/show/days/{year}/{month}/{plant.key}')
        print(f'fetching yield on {month}/{year} for {plant}')
        ans = self.get_result(requests.get(uri).json())
        return [DayYield(parse(y['day']).date(), y['watthours']) for y in ans['yields'] ]

    def show_year(self, plant: Plant, year: Integral = None) -> List[DayYield]:
        if year is None:
            year = date.today().year
        uri = self.make_url(f'plant/yield/show/days/{year}/{plant.key}')
        print(f'fetching {year} yields for {plant}')
        ans = self.get_result(requests.get(uri).json())
        return [ DayYield(parse(y['date']).date(), y['watthours']) for y in ans['yields'] ]
