from datetime import date

from dateutil.parser import parse


class Plant:

    def __init__(self, name: str, key: str, first_entry: date):
        self.name = name
        self.key = key
        self.first_entry = first_entry

    def __str__(self):
        return self.name

    @staticmethod
    def from_json(data):
        name = data['name']
        key = data['plant_key']
        first_day = parse(data['first_day']).date()
        return Plant(name, key, first_day)