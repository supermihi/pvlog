from datetime import timedelta, date
from numbers import Integral
from pathlib import Path
from typing import Tuple, List, Dict

from openpyxl.worksheet.worksheet import Worksheet
import openpyxl

from client import PvLogClient, PvLogError
from db import PlantDatabase
from plant import Plant


class PvExcelDatabase:

    def __init__(self, path: Path):
        self.path = path
        self.book = openpyxl.load_workbook(str(path))
        self.sheet = self.book[self.book.sheetnames[0]]

    def store_yields(self, dbs: List[PlantDatabase]):
        min_date = min(min(db.yields).date for db in dbs)
        max_date = max(max(db.yields).date for db in dbs)
        file_dates = []
        day = min_date
        while day <= max_date:
            file_dates.append(day)
            day = day + timedelta(days=1)

        self.sheet['A1'] = 'Tag'
        self.sheet['A2'] = 'Ertrag (Wh)'
        for row, day in enumerate(file_dates, start=2):
            cell = self.sheet.cell(row=row, column=1)
            cell.value = day
            cell.number_format = 'DD.MM.YYYY'

        date_to_row = {day: row for row, day in enumerate(file_dates, start=2)}
        for col, db in enumerate(dbs, start=2):
            self.store_plant_yields(col, db, date_to_row)

    def store_plant_yields(self, col: int, db: PlantDatabase, date_to_row: Dict[date, int]):
        self.sheet.cell(row=1, column=col).value = db.plant.name
        for yld in db.yields:
            row = date_to_row[yld.date]
            self.sheet.cell(row=row, column=col).value = yld.watthours

    def save(self):
        self.book.save(str(self.path))


class PlantSheet:

    def __init__(self, plant: Plant, sheet: Worksheet):
        self.plant = plant
        self.sheet = sheet

    def last_entry(self) -> Tuple[date, Integral]:
        date_col = self.sheet['A0:']
        if len(date_col) > 1:
            return date_col[-1].value.date(), date_col[-1].row
        return self.plant.first_entry - timedelta(days=1), 0

    def missing_days(self, last: date):
        missing = []
        target = date.today() - timedelta(days=1)
        if last <= target:
            day = last + timedelta(days=1)
            while day <= target:
                missing.append(day)
                day = day + timedelta(days=1)
        return missing

    def fetch_missing(self, client: PvLogClient):
        last_day, last_row = self.last_entry()
        row = last_row + 1
        missing_dates = self.missing_days(last_day)
        fetched_yields = {}
        missing_months = set((date.year, date.month) for date in missing_dates)
        for year, month in missing_months:
            try:
                fetched = client.show_month(self.plant, year, month)
            except PvLogError as e:
                print(e)
                return
            for date, value in fetched.items():
                fetched_yields[date] = value
        for missing in missing_dates:
            watthours = fetched_yields.get(missing, 0)
            date_cell = self.sheet.cell(row=row, column=1)
            date_cell.value = missing
            date_cell.number_format = 'DD.MM.YYYY'
            self.sheet.cell(row=row, column=2).value = watthours
            row = row + 1
